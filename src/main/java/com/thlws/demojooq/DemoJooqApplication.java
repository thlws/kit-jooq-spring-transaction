package com.thlws.demojooq;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
@ServletComponentScan({"com.thlws.demojooq.servlet","com.thlws.demojooq.filter"})
@EnableTransactionManagement
public class DemoJooqApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoJooqApplication.class, args);
    }
}
