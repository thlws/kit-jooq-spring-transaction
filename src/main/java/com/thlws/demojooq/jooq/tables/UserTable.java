/*
 * This file is generated by jOOQ.
 */
package com.thlws.demojooq.jooq.tables;


import com.thlws.demojooq.jooq.Db1;
import com.thlws.demojooq.jooq.Indexes;
import com.thlws.demojooq.jooq.Keys;
import com.thlws.demojooq.jooq.records.UserRecord;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

import javax.annotation.Generated;

import org.jooq.Field;
import org.jooq.ForeignKey;
import org.jooq.Identity;
import org.jooq.Index;
import org.jooq.Name;
import org.jooq.Record;
import org.jooq.Schema;
import org.jooq.Table;
import org.jooq.TableField;
import org.jooq.UniqueKey;
import org.jooq.impl.DSL;
import org.jooq.impl.TableImpl;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.11.7"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class UserTable extends TableImpl<UserRecord> {

    private static final long serialVersionUID = 678640860;

    /**
     * The reference instance of <code>db1.user</code>
     */
    public static final UserTable USER = new UserTable();

    /**
     * The class holding records for this type
     */
    @Override
    public Class<UserRecord> getRecordType() {
        return UserRecord.class;
    }

    /**
     * The column <code>db1.user.id</code>.
     */
    public final TableField<UserRecord, Integer> ID = createField("id", org.jooq.impl.SQLDataType.INTEGER.nullable(false).identity(true), this, "");

    /**
     * The column <code>db1.user.username</code>.
     */
    public final TableField<UserRecord, String> USERNAME = createField("username", org.jooq.impl.SQLDataType.VARCHAR(20), this, "");

    /**
     * The column <code>db1.user.password</code>.
     */
    public final TableField<UserRecord, String> PASSWORD = createField("password", org.jooq.impl.SQLDataType.VARCHAR(20), this, "");

    /**
     * The column <code>db1.user.gender</code>.
     */
    public final TableField<UserRecord, String> GENDER = createField("gender", org.jooq.impl.SQLDataType.CHAR(1), this, "");

    /**
     * The column <code>db1.user.birthday</code>.
     */
    public final TableField<UserRecord, LocalDate> BIRTHDAY = createField("birthday", org.jooq.impl.SQLDataType.LOCALDATE, this, "");

    /**
     * The column <code>db1.user.create_time</code>.
     */
    public final TableField<UserRecord, LocalDateTime> CREATE_TIME = createField("create_time", org.jooq.impl.SQLDataType.LOCALDATETIME, this, "");

    /**
     * Create a <code>db1.user</code> table reference
     */
    public UserTable() {
        this(DSL.name("user"), null);
    }

    /**
     * Create an aliased <code>db1.user</code> table reference
     */
    public UserTable(String alias) {
        this(DSL.name(alias), USER);
    }

    /**
     * Create an aliased <code>db1.user</code> table reference
     */
    public UserTable(Name alias) {
        this(alias, USER);
    }

    private UserTable(Name alias, Table<UserRecord> aliased) {
        this(alias, aliased, null);
    }

    private UserTable(Name alias, Table<UserRecord> aliased, Field<?>[] parameters) {
        super(alias, null, aliased, parameters, DSL.comment(""));
    }

    public <O extends Record> UserTable(Table<O> child, ForeignKey<O, UserRecord> key) {
        super(child, key, USER);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Schema getSchema() {
        return Db1.DB1;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Index> getIndexes() {
        return Arrays.<Index>asList(Indexes.USER_PRIMARY);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Identity<UserRecord, Integer> getIdentity() {
        return Keys.IDENTITY_USER;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UniqueKey<UserRecord> getPrimaryKey() {
        return Keys.KEY_USER_PRIMARY;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<UniqueKey<UserRecord>> getKeys() {
        return Arrays.<UniqueKey<UserRecord>>asList(Keys.KEY_USER_PRIMARY);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UserTable as(String alias) {
        return new UserTable(DSL.name(alias), this);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UserTable as(Name alias) {
        return new UserTable(alias, this);
    }

    /**
     * Rename this table
     */
    @Override
    public UserTable rename(String name) {
        return new UserTable(DSL.name(name), null);
    }

    /**
     * Rename this table
     */
    @Override
    public UserTable rename(Name name) {
        return new UserTable(name, null);
    }
}
