package com.thlws.demojooq.service;

import com.thlws.demojooq.jooq.daos.UserDao;
import com.thlws.demojooq.jooq.pojos.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

/**
 * Created by  HanleyTang on 2019-01-05
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class UserService {

    @Autowired
    private UserDao userDao;

    @Transactional
    public void saveUser(){
        User user = new User();
        user.setBirthday(LocalDate.now());
        user.setCreateTime(LocalDateTime.now());
        user.setPassword("123456");
        user.setUsername("hanleytang");
        userDao.insert(user);
    }

    @Transactional
    public void saveUser2(){
        User user = new User();
        user.setBirthday(LocalDate.now());
        user.setCreateTime(LocalDateTime.now());
        user.setPassword("123456");
        user.setUsername("模拟报错,不该出现！");
        userDao.insert(user);
        int r = 0/0;
    }

    public List<User> loadUsers(){
        return userDao.findAll();
    }

}
