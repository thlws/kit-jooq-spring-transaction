package com.thlws.demojooq.config;

import org.jooq.ExecuteListener;
import org.jooq.SQLDialect;
import org.jooq.impl.DataSourceConnectionProvider;
import org.jooq.impl.DefaultConfiguration;
import org.jooq.impl.DefaultDSLContext;
import org.jooq.impl.DefaultExecuteListenerProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.jdbc.datasource.TransactionAwareDataSourceProxy;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

/**
 * 本文配置内容参考官方说明 http://www.jooq.org/doc/3.11/manual-single-page/#jooq-with-spring
 * JOOQ 数据源及事务配置
 * @author   HanleyTang on 2019-01-12
 */
@Configuration
public class SpringConfig {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    private final DbConfig dbConfig;

    @Autowired
    public SpringConfig(DbConfig dbConfig) {
        this.dbConfig = dbConfig;
    }

    @PostConstruct
    private void init() {
        logger.debug("初始化db信息......");
    }




    @Bean("jooqDataSource")
    public TransactionAwareDataSourceProxy transactionAwareDataSourceProxy(){

        DataSource dataSource = DataSourceBuilder.create()
                .driverClassName(dbConfig.getDriver_class_name())
                .password(dbConfig.getPassword())
                .username(dbConfig.getUsername())
                .url(dbConfig.getUrl()).build();

        return new TransactionAwareDataSourceProxy(dataSource);
    }



    @Bean(name = "transactionManager")
    public DataSourceTransactionManager transactionManager(@Qualifier("jooqDataSource") TransactionAwareDataSourceProxy jooqDataSource){
        return new DataSourceTransactionManager(jooqDataSource);
    }


    @Bean("jooqConfiguration")
    public DefaultConfiguration jooqConfiguration(@Qualifier("jooqDataSource") DataSource jooqDataSource ){

        ExecuteListener executeListener = new ExceptionTranslator();
        DataSourceConnectionProvider myConnectionProvider = new DataSourceConnectionProvider(jooqDataSource);
        DefaultExecuteListenerProvider executeListenerProvider = new DefaultExecuteListenerProvider(executeListener);

        DefaultConfiguration configuration = new DefaultConfiguration();
        configuration.setConnectionProvider(myConnectionProvider);
        configuration.setExecuteListenerProvider(executeListenerProvider);
        configuration.setSQLDialect(SQLDialect.MYSQL);
        return configuration;
    }

    @Bean("myDSLContext")
    public DefaultDSLContext jooqDSLContext(@Qualifier("jooqConfiguration") DefaultConfiguration configuration ){

        return new DefaultDSLContext(configuration);
    }


}
