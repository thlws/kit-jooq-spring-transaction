package com.thlws.demojooq.filter;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import java.io.IOException;

/**
 * Created by HanleyTang on 2018/6/27
 *
 * @author Hanley[hanley@thlws.com]
 * @version 1.0
 */
@WebFilter(filterName = "addonFilter", urlPatterns = "/*")
public class AddonFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        System.out.println("filterConfig = [" + filterConfig + "]");
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        System.out.println("request = [" + request + "], response = [" + response + "], chain = [" + chain + "]");
        chain.doFilter(request,response);
    }

    @Override
    public void destroy() {

    }
}
